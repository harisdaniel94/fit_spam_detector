##Zadání projektu##

Vytvoře program pro detekci spamu v emilové komunikaci. Možno použít jazyky C,C++, Python. 

Po provedení příkazu "make" bude k dispozici spustitelný soubor "antispam". Program bude přijímat volitelné množství parametrů, kde každý parametr obsahuje cestu k souboru s emailem. Emailový soubor bude ve formátu .eml. Pro každý vstup soubor program vytiskne na standardní výstup: 
<název souboru> - <hodnocení> - důvod hodnocení \n 

možné hodnocení:
OK - email není spam 
SPAM - email je spam 
FAIL - nepodařilo se ohodnotit (selhání programu apod.) 

Uvedení důvodu hodnocení není povinné, nebude mít vliv na hodnocení projektu. Může k němu však být přihlédnuto v případě nejasností/reklamací apod. 
Příklad spuštění: 
./antispam email.eml email2.eml email3.eml email4.eml 
email.eml - OK 
email2.eml - SPAM - contains word sex 
email3.eml - SPAM 
email4.eml - FAIL - failed to open email file 
Metoda hodnocení je na vás, jako inspiraci můžete použít třeba: http://spamassassin.apache.org/old/tests_3_3_x.html.

Jako příklady vstupních souborů můžete použít tyto. 

Můžete použít metody založené na učení, avšak po odevzdání se váš antispam již učit nesmí. Tedy pokud jej spustíme n-krát se stejnou testovací sadou musí dát stejné výsledky. 

Váš program se nesmí přípojovat k síti, ani používat jiné zdroje, které nejsou zahrnuty ve vašem projektu. 

Pokud se rozhodnete použít knihovnu pro parsování .eml souborů, zmiňte ji v dokumentaci. Pamatuje však na to, že projekt musí být spustitelný na serveru merlin. Pokud zjistíte že na merlinovi chybí nejaká standardní knihovna, napište do fóra a zkusíme to vyřešit.

##Hodnocení:##

Projekt bude ohodnocen podle množství detekovaných spamů vs. false pozitives. Položka spam označuje množství správně označených spamů, položka ham označuje množství správně označený regulerních emailů:
1b - 20% spam, 80% ham 
2b - 23% spam, 83% ham 
3b - 26% spam, 85% ham 
4b - 30% spam, 87% ham 
5b - 34% spam, 89% han 
6b - 37% spam, 91% ham 
7b - 40% spam, 93% ham 
8b - 45% spam, 95% ham 


Z projektu je možné získat až 2 bonusové body, které obdržíte v případě, že váš antispam korektně detekuje velmi vysoké množství spamů. 
- 2 bonusé body obdrží 3 nejlepší antispamy.
- 1 bonusový bod obdrží dalších 5 v pořadí. 
Tyto bonusové body dostanete připočtené k bodům ze závěrečné zkoušky. 

V případě nedostatečné/chybějící dokumentace či pochybných zdrojových kódů může být projekt ohodnocen 0 body.

##Dokumentace:##

K vašemu projektu vypracujte krátkou dokumentaci, ve které popište na základě čeho a jakým způsobem detekujete spamy. Očekavaný rozsah je 1-3 strany. 

Pokud váš program využívá knihovnu pro parsování emailů nebo pro podporu zpracování spamů, zmiňte ji v dokumentaci.
Odevzdání:

Termín odevzdání: 15.12.2017 

Odevzdávejte archiv xlogin00.zip který bude obsahovat: 
- Zdrojové kódy programu 
- makefile 
- dokumentaci ve formátu .pdf 

Program musí být možné přeložit a spustit na serveru merlin.