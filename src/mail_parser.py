# # # # # # # # # # # # # #
# project: spam_detector
#    file: mail_parser
#  author: Daniel Haris
# # # # # # # # # # # # # #

from os import listdir
from re import sub as re_sub
from email import message_from_file
from nltk import PorterStemmer
from nltk.corpus import stopwords as nltk_stopwords
from random import sample as random_sample
from html2text import HTML2Text


class MailParser:
    """
    Class responsible for preprocessing input emails
    """

    html_to_text_converter = HTML2Text()
    html_to_text_converter.ignore_links = True
    html_to_text_converter.ignore_emphasis = True
    html_to_text_converter.images_to_alt = True
    html_to_text_converter.single_line_break = True

    text_transformations = [
        (r'\b[\w\-.]+?@\w+?\.\w{2,4}\b', 'emailaddr'),
        (r'(http[s]?\S+)|(\w+\.[A-Za-z]{2,4}\S*)', 'httpaddr'),
        (r'£|\$|\€', 'moneysymb'),
        (r'\b(\+\d{1,2}\s)?\d?[\-(.]?\d{3}\)?[\s.-]?\d{3}[\s.-]?\d{4}\b', 'phonenumbr'),
        (r'\d+(\.\d+)?', 'numbr'),
        (r'[^\w\d\s]', ' '),
        (r'\s+', ' '),
        (r'^\s+|\s+?$', ''),  # the same as trim/strip
        (r'(daniel|haris|harisdaniel94|xharis00|enron)', ''),  # remove personal information from emails
        (r'(martin|cernek|černek|peter|strecansky|strečanský)', '')
    ]

    stop_words = nltk_stopwords.words("english")

    def __init__(self, emails_path):
        self.emails_path = emails_path
        self.selected_email_paths_with_labels = []

    @staticmethod
    def _listdir(path) -> iter:
        """
        Method lists folder content without hidden files and TXT files
        :param path: folder path 
        :return: content of folder
        """
        for file in listdir(path):
            if not file.startswith("."):
                yield file

    def create_dataset(self, ham_count, spam_count) -> list:
        """
        Method return list of parsed emails (dictionaries)
        :param ham_count: ham email count
        :param spam_count: spam email count
        :return: list of parsed emails
        """
        mails = []
        labels = []
        for folder in self._listdir(self.emails_path):
            if folder in ["ham", "spam"]:
                email_paths = self.get_mails(
                    "{}/{}".format(self.emails_path, folder),
                    0 if folder == "ham" else 1,
                    ham_count if folder == "ham" else spam_count
                )
            elif folder in ["other_ham", "other_spam"]:
                label = folder.split("_")[1]
                email_paths = self.get_mails(
                    "{}/{}".format(self.emails_path, folder),
                    0 if label == "ham" else 1,
                    None
                )
            else:
                continue
            self.selected_email_paths_with_labels.extend(email_paths)
        for email_path, label in self.selected_email_paths_with_labels:
            try:
                mails.append(self.parse_mail(email_path))
                labels.append(label)
            except:
                print(">> {!r} in {!r} could not be parsed".format(email_path, label))
        return mails, labels

    def get_mails(self, mail_folder_path: str, label: int, many: int) -> list:
        """
        Method returns 'many' randomly selected emails from email folder
        :param mail_folder_path: folder where mails are
        :param label: ham (0) or spam (1)
        :param many: number of randomly selected emails
        :return: list of parsed mails
        """
        all_email_paths = [("{}/{}".format(mail_folder_path, email_path), label)
                           for email_path in self._listdir(mail_folder_path)]
        return all_email_paths if not many else random_sample(all_email_paths, many)

    def preprocess_text(self, mail: str) -> str:
        """
        Replace some tokens in mail
        :param mail: mail in string
        """
        # normalization - replace mails, web urls, phone numbers, ...
        for t in self.text_transformations:
            mail = re_sub(*t, mail)
        # eliminating stop words
        mail = " ".join(term for term in mail.split() if term not in set(self.stop_words))
        # stemming
        porter = PorterStemmer()
        mail = " ".join(porter.stem(term) for term in mail.split())
        return mail

    def parse_mail(self, mail_path: str) -> str:
        """
        Method parses mail to internal representation
        :param mail_path: path to mail
        :return: list of dictionary (email)
        """
        with open(mail_path, 'r', encoding="latin2") as fr:
            b = message_from_file(fr)
            subject = b["subject"]
            if subject is None:
                subject = ""
            body = ""
            if b.is_multipart():
                for part in b.walk():
                    ctype = part.get_content_type()
                    cdispo = str(part.get('Content-Disposition'))
                    # skip any text/plain (txt) attachments
                    if ctype == 'text/plain' and 'attachment' not in cdispo:
                        body = part.get_payload(decode=True)  # decode
                        break
            else:
                body = b.get_payload(decode=False)
            if type(body) != str:
                body = body.decode("latin2")
            # convert to lower
            subject = subject.lower()
            body = body.lower()
            if "<html>" in body:
                # remove CSS code
                body = re_sub(r'{.*?}', '', body)
                # extract text from HTML code
                body = self.html_to_text_converter.handle(body)
            return self.preprocess_text(subject + " " + body)
