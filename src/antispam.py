#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# # # # # # # # # # # # # #
# project: spam_detector
#    file: antispam
#  author: Daniel Haris
# # # # # # # # # # # # # #

import sys
import numpy as np
import pickle
from mail_parser import MailParser
from sklearn.feature_extraction.text import TfidfVectorizer
from model_selection import ModelSelection


class AntiSpam:
    """
    Class responsible for:
    - loading emails
    - preprocessing
    - training or testing
    """

    emails_path = "./emails"
    clf_pickle_path = "clf.pkl"
    tkn_pickle_path = "tkn.pkl"

    def __init__(self, input_email_paths=None):
        self.mail_parser = MailParser(self.emails_path)
        self.input_email_paths = np.array(input_email_paths)

    def dump_model_to_pickle(self, model):
        """
        Method "freezes" a classification model into a pickle file
        :param model: classification model object
        """
        with open(self.clf_pickle_path, "wb") as fw:
            pickle.dump(model, fw)

    def dump_tokenizer_to_pickle(self, tokenizer):
        """
        Method "freezes" a tokenizer into a pickle file
        :param tokenizer: tokenizer
        """
        with open(self.tkn_pickle_path, "wb") as fw:
            pickle.dump(tokenizer, fw)

    def load_model_from_pickle(self):
        """
        Method loads serialized classification model in pickle formats
        :return: classification model object
        """
        try:
            with open(self.clf_pickle_path, "rb") as fr:
                return pickle.load(fr)
        except FileNotFoundError:
            raise FileNotFoundError(
                "File {!r} containing the serialized classifier was not found".format(AntiSpam.clf_pickle_path))

    def load_tokenizer_from_pickle(self):
        """
        Method loads serialized tokenizer in pickle formats
        :return: tokenizer
        """
        try:
            with open(self.tkn_pickle_path, "rb") as fr:
                return pickle.load(fr)
        except FileNotFoundError:
            raise FileNotFoundError(
                "File {!r} containing the serialized tokenizer was not found".format(AntiSpam.clf_pickle_path))

    @staticmethod
    def create_tfidf_matrix(data) -> tuple:
        """
        Method creates TF-IDF matrix from given email set
        - TF-IDF matrix is the result of the tokenizing process
        :param data: email set
        :return: TF-IDF matrix, tokenizer object
        """
        tokenizer = TfidfVectorizer(ngram_range=(1, 2))
        tokenizer.fit(data)
        return tokenizer.transform(data), tokenizer

    def train(self, ham_count, spam_count):
        """
        Method is responsible for
        - preprocessing
        - tf-idf matrix creation
        - cross validation model selection
        - fitting the best model
        - serializing model and tf-idf matrix
        :param ham_count: count of "ham" mails
        :param spam_count: count of "spam" mail
        """
        # get data and labels
        mails, labels = self.mail_parser.create_dataset(ham_count, spam_count)
        # convert to numpy arrays - sklearn machine learning algorithms work with them
        labels = np.array(labels)
        mails = np.array(mails)
        # tokenizing process - create TF-IDF matrix
        X_ngrams, tkn = self.create_tfidf_matrix(mails)
        self.show_most_important_features(tkn, X_ngrams, 40)
        # choose model with best performance
        ms = ModelSelection(X_ngrams, labels)
        clf = ms.get_best_model()
        # dump classifier and tokenizer into pickle
        self.dump_model_to_pickle(clf["SVC"])  # use SVC classifier
        self.dump_tokenizer_to_pickle(tkn)

    @staticmethod
    def show_most_important_features(tfidf_obj, tfidf_matrix, many):
        """
        Method prints out "many" most important features
        :param tfidf_obj: tfidf vectorizer object
        :param tfidf_matrix: tfidf vectorizer matrix
        :param many: how many features to print
        """
        # http://stackoverflow.com/questions/16078015/
        scores = zip(tfidf_obj.get_feature_names(), np.asarray(tfidf_matrix.sum(axis=0)).ravel())
        sorted_scores = sorted(scores, key=lambda x: x[1], reverse=True)
        for item in sorted_scores[:many]:
            print("{0:50} Score: {1}".format(item[0], item[1]))

    def detect_spam(self):
        """
        Method classifies given email set into ham or spam
        """
        def decider(pred) -> int:
            """
            Method decides whether classification is more likely a ham or spam
            :param pred: classification probability in form [p_ham, p_spam]
            :return: 0 (ham) or 1 (spam) 
            """
            return 0 if pred[0] >= 0.4 else 1

        # parse input emails (parsing, cleaning, normalizing, eliminating stop words, stemming)
        parsed_mails = []
        for email_path in self.input_email_paths:
            try:
                parsed_mails.append([self.mail_parser.parse_mail(email_path)])
            except:
                parsed_mails.append(None)
        clf = self.load_model_from_pickle()  # load classifier
        tkn = self.load_tokenizer_from_pickle()  # load tokenizer
        # get predicted (classified) labels
        for parsed_mail, mail_path in zip(parsed_mails, self.input_email_paths):
            if parsed_mail is None:  # problem with mail parsing
                print("{} - FAIL".format(mail_path))
            else:  # classification
                X = tkn.transform(parsed_mail)  # get feature vector
                clf_result = clf.predict_proba(X)
                pred_label = decider(clf_result[0])  # get label
                print("{} - {}".format(mail_path, "OK" if pred_label == 0 else "SPAM"))


def print_training_usage():
    print("Usage: ./antispam --train <ham_count> <spam_count>")
    exit(1)


def print_testing_usage():
    print("Usage: ./antispam <email_path> (<email_path>)*")
    exit(1)

if __name__ == "__main__":
    ham_count, spam_count = 0, 0
    if len(sys.argv) > 1 and sys.argv[1] == "--train":
        if len(sys.argv) == 4:
            try:
                ham_count = int(sys.argv[2])
                spam_count = int(sys.argv[3])
            except ValueError:
                print_training_usage()
            antispam = AntiSpam(None)
            antispam.train(ham_count=ham_count, spam_count=spam_count)
        else:
            print_training_usage()
    else:
        if len(sys.argv) > 1:
            antispam = AntiSpam(sys.argv[1:])
            antispam.detect_spam()
        else:
            print_testing_usage()
