#!/usr/bin/env python3
#-*- coding: utf-8 -*-
# # # # # # # # # # # # # #
# project: spam_detector
#    file: model_selection
#  author: Daniel Haris
# # # # # # # # # # # # # #

import numpy as np
import pandas as pd
from sklearn.naive_bayes import MultinomialNB, BernoulliNB
from sklearn.svm import SVC
# from sklearn.tree import DecisionTreeClassifier
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import StratifiedShuffleSplit


class EstimatorSelectionHelper:
    """
    Class is responsible for cross validation along with hyper-parameter tuning
    """

    def __init__(self, models, params):
        missing_params = list(set(models.keys()) - set(params.keys()))
        if missing_params:
            raise ValueError("Some estimators are missing parameters: %s" % missing_params)
        self.models = models
        self.params = params
        self.keys = models.keys()
        self.best_models = {}

    def fit(self, X, y):
        """
        Method fits all models with all combinations of hyper-parameter settings and returns the bet model
        with the best combination of hyper-parameters (Grid Search)
        :param X: features
        :param y: labels
        """
        for key in self.keys:
            print("Running GridSearchCV for {}".format(key))
            model = self.models[key]
            params = self.params[key]
            gs = GridSearchCV(
                model,
                params,
                cv=StratifiedShuffleSplit(n_splits=10, test_size=0.2, random_state=42),
                n_jobs=-2,
                scoring="accuracy",
                refit=True)
            gs.fit(X, y)
            self.best_models[key] = gs

    def score_summary(self, sort_by='mean_score'):
        """
        Method returns cross validation scores summary in CSV file
        :param sort_by: sort key
        """
        def row(key, scores, params):
            d = {
                'estimator': key,
                'min_score': np.min(scores),
                'max_score': np.max(scores),
                'mean_score': np.mean(scores),
                'std_score': np.std(scores),
            }
            return pd.Series({**params, **d})

        rows = [row(k, gsc.cv_validation_scores, gsc.parameters)
                for k in self.keys
                for gsc in self.best_models[k].grid_scores_]
        df = pd.concat(rows, axis=1).T.sort_values(by=[sort_by], ascending=False)
        columns = ['estimator', 'min_score', 'mean_score', 'max_score', 'std_score']
        columns = columns + [c for c in df.columns if c not in columns]
        df[columns].to_csv("scores_new.csv", index=False)


class ModelSelection:
    """
    Class is responsible for selecting the best model with cross validation, performs hyper-parameter tuning
    """
    def __init__(self, X, y):
        self.X = X  # X = features
        self.y = y  # y = label

    def get_best_model(self):
        """
        Method returns best model from "models" with best setting
        """
        models = {
            "BernoulliNB": BernoulliNB(),
            "MultinomialNB": MultinomialNB(),
            "SVC": SVC()  # ,
            # add more
        }
        params = {
            "BernoulliNB": {
                "alpha": [0.01, 0.2, 0.5, 1.0],
                "fit_prior": [True, False]
            },
            "MultinomialNB": {
                "alpha": [0.01, 0.2, 0.5, 1.0],
                "fit_prior": [True, False]
            },
            "SVC": {
                "kernel": ["linear"],
                "C": [10, 100],
                "gamma": [0.0001],
                "probability": [True]
            }
        }
        # retrieve best model as result of cross validation with hyper-parameter tuning
        estimator_helper = EstimatorSelectionHelper(models, params)
        estimator_helper.fit(self.X, self.y)
        print(estimator_helper.score_summary(sort_by="min_score"))
        return estimator_helper.best_models
